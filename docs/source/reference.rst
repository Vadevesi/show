====================
Show class reference
====================
.. autoclass:: show.ShowPrinter
   :members:

    .. automethod:: __init__