============
Installation
============

Prerequisites
=============

This program requires the following dependencies:

* ``python=3.9.9``: as this is a python program
* ``pip``: for installing python packages
* ``wget``: for retrieving ONNX files from the internet
* ``graphviz``: for outputting SVG files of graphs
* ``virtualenv`` (optional) for installing all packages in a virtual environment

To also build the documentation, you need:

* ``sphinx``: the python documentation generator.
* ``make``: the GNU Make Build tool.

Installation
============

Setting up a virtual environment can happen as follows:

.. code-block:: sh

    pip install virtualenv
    virtualenv -p 39 venv
    source venv/bin/activate

Now this shell session is mapped to your virtual environment named ``venv``.

Now you can proceed with installing the right packages inside your environment:

.. code-block:: sh

    pip install -r requirements.txt

Building the documentation
==========================

To also install the requirements for building the documentation, you can run:

.. code-block:: sh

    pip install -r docs/requirements.txt
    cd docs
    make html