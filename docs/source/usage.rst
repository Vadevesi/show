=====
Usage
=====

Get an ONNX file
================
To start parsing and analyzing an ONNX file, we first need one.
One place to get ONNX files is the `ONNX model zoo <https://github.com/onnx/models>`_

Here we get a neural network for digit (MNIST) classification:

.. code-block:: sh

    wget https://github.com/onnx/models/raw/master/vision/classification/mnist/model/mnist-1.onnx


Run  the main script
====================

After the main script is adapted, the script can now be ran from the show folder:

.. code-block:: sh

    python show.py ../mnist-1.onnx

For the MNIST example, you should observe the following output:

.. code-block:: text

    Constant377
    Block386
    Constant321
    Convolution28
    Constant318
    Reshape398
    Plus30_reshape1
    Plus30
    ReLU32
    Pooling66
    Constant340
    Convolution110
    Constant346
    Reshape408
    Plus112_reshape1
    Plus112
    ReLU114
    Pooling160
    Times212_reshape0
    Constant312
    Times212_reshape1
    Times212

Notice that now also an SVG file is generated called ``Digraph.gv.svg``
This file should look something like this:

.. image:: Digraph.gv.svg
    :width: 600


Great! Now you know all the basics of using Show!